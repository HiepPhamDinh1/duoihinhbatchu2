package com.example.duoihinhbatchu;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.duoihinhbatchu.view.frag.M001SplashFragment;
import com.example.duoihinhbatchu.view.frag.M002MenuFragment;
import com.example.duoihinhbatchu.view.frag.M003MainFragment;
import com.example.duoihinhbatchu.view.frag.M004HighScoreFragment;

public class MainActivity extends AppCompatActivity implements OnActionCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        StatusBarUtil.setTransparent(this);

        M001SplashFragment m001SplashFragment = new M001SplashFragment();
        m001SplashFragment.setOnActionCallBack(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_view,m001SplashFragment);
        transaction.commit();
    }

    @Override
    public void onCallBack(String key, Object object) {
        switch (key){
            case M001SplashFragment.KEY_MENUFRAGMENT:
                M002MenuFragment m002MenuFragment = new M002MenuFragment();
                m002MenuFragment.setOnActionCallBack(this);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container_view,m002MenuFragment);
                transaction.commit();
                break;
            case M002MenuFragment.KEY_MAINFRAGMENT:
                M003MainFragment m003MainFragment = new  M003MainFragment();
                m003MainFragment.setOnActionCallBack(this);
                FragmentTransaction transaction1 = getSupportFragmentManager().beginTransaction();
                transaction1.replace(R.id.container_view,m003MainFragment);
                transaction1.addToBackStack("add");
                transaction1.commit();
                break;
            case M003MainFragment.KEY_HIGHSCORE:
                M004HighScoreFragment m004HighScoreFragment = new M004HighScoreFragment();
                FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                transaction2.replace(R.id.container_view,m004HighScoreFragment);
                transaction2.addToBackStack("add");
                transaction2.commit();
                break;

        }
    }
}