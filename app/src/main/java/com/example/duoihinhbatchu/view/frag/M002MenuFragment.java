package com.example.duoihinhbatchu.view.frag;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.duoihinhbatchu.OnActionCallBack;
import com.example.duoihinhbatchu.R;

public class M002MenuFragment extends Fragment {
    public static final String KEY_MAINFRAGMENT = "KEY_MAINFRAGMENT";
    private View view;
    private ImageView imvPlay , imvSound;
    int i = 0;
    private MediaPlayer mediaPlayer;
    private OnActionCallBack onActionCallBack;

    public void setOnActionCallBack(OnActionCallBack onActionCallBack) {
        this.onActionCallBack = onActionCallBack;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_m002_menu,container,false);
        initViews();
        initEvents();
        return view;
    }

    private void initEvents() {
        // 1. Xu ly sound on/off
        mediaPlayer.start();
        imvSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i++;
                if(i%2==1){
                    imvSound.setImageResource(R.drawable.ic_sound_off);
                    mediaPlayer.pause();
                }else{
                    imvSound.setImageResource(R.drawable.ic_sound_on);
                    mediaPlayer.start();
                }

            }
        });

        // 2. Xu ly play
        imvPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onActionCallBack.onCallBack(KEY_MAINFRAGMENT,null);
            }
        });
    }

    private void initViews() {
        imvPlay = view.findViewById(R.id.imv_play);
        imvSound = view.findViewById(R.id.imv_sound);
        mediaPlayer = MediaPlayer.create(getContext(),R.raw.raw_sound);
    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.release();
    }
}
