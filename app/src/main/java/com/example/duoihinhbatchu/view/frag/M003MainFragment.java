package com.example.duoihinhbatchu.view.frag;

import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.duoihinhbatchu.OnActionCallBack;
import com.example.duoihinhbatchu.OnClickCallBack;
import com.example.duoihinhbatchu.R;
import com.example.duoihinhbatchu.model.TopicPicture;
import com.example.duoihinhbatchu.model.TopicTitle;
import com.example.duoihinhbatchu.view.adapter.SquareHiddenAdapter;
import com.example.duoihinhbatchu.view.adapter.SquareShowAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class M003MainFragment extends Fragment {
    public static final String KEY_HIGHSCORE = "KEY_HIGHSCORE";
    private View view;
    private ImageView imvBack , imvPicture;
    private List<TopicPicture> listTopicPicture;
    private List<TopicTitle> listTopicTitle;
    private List<String> listSquareHidden , listSquareShow;
    private RecyclerView rvSquareHidden , rvSquareShow;
    private SquareHiddenAdapter squareHiddenAdapter;
    private SquareShowAdapter squareShowAdapter;
    private int j = 0;
    public static String s2 = "";
    public static String s = "";
    public static int k = 0;
    public static int point = 814;
    private OnActionCallBack onActionCallBack;
    private TextView tvPoint;

    public void setOnActionCallBack(OnActionCallBack onActionCallBack) {
        this.onActionCallBack = onActionCallBack;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_m003_main,container,false);
        initData();
        initViews();
        initEvents();
        return view;
    }

    private void initData() {
        // Lay ra danh sach anh trong assets va gan vao list
        listTopicPicture = new ArrayList<>();
        try {
            String[] listPath = getContext().getAssets().list("icon");
            for (String listName : listPath){
                String iconName = "icon/"+listName;
                listTopicPicture.add(new TopicPicture(iconName));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Lay ra text cua dapan.txt;
        listTopicTitle = new ArrayList<>();
        String str = "";
        AssetManager assetManager = getContext().getAssets();
        try {
            InputStream inputStream = assetManager.open("data/dapan.txt");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            str = new String(buffer);
            inputStream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

        // Lay ra danh sach cac cau tra loi va cho vao list
        String[] listTitle = str.split("\n");
        for(int i=0;i<listTitle.length;i++){
            String s5 = listTitle[i].trim();
            listTopicTitle.add(new TopicTitle(s5));
        }


    }

    private void initEvents() {
        tvPoint.setText(""+point);
        j = 0;
        s2 = "";
        imvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        // Dùng Glide để cho ảnh lên
        try {
            Glide.with(getContext()).load(BitmapFactory.decodeStream(getContext().getAssets().open(listTopicPicture.get(k).getmPicture()))).into(imvPicture);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Hien thi cac chữ cái trong câu trả lời bằng RecyclerView
        listSquareHidden =  new ArrayList<>();
        int length = listTopicTitle.get(k).getmTitle().length();
        s = listTopicTitle.get(k).getmTitle();
          for(int i=0;i<length;i++){
              listSquareHidden.add("?");
          }
        squareHiddenAdapter = new SquareHiddenAdapter(getContext(),listSquareHidden);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(getContext(),8);
        rvSquareHidden.setLayoutManager(linearLayoutManager);
        rvSquareHidden.setAdapter(squareHiddenAdapter);

        // Hien thi cac chu cai trong cho dien bằng RecyclerView
        listSquareShow = new ArrayList<>();
        Random random = new Random();
        String s1 = s;
        for(int i = 0;i<1000;i++){               // Lay them cac phan tu de du 14 chu
            int count = Math.abs(random.nextInt(89));
            if(count>=65){
                s1+=(char)count+"";
                if(s1.length()==14){
                    break;
                }
            }
        }

        char[] ch = new char[14];
        for(int i=0;i<s1.length();i++){   // Cho vao list de hien len Recyclerview
            char c = s1.charAt(i);
            ch[i] = c;
        }
        Arrays.sort(ch); // Sort list char de dao loan cac phan tu
        for(int i=0;i<s1.length();i++){
            listSquareShow.add(ch[i]+"");
        }
        squareShowAdapter = new SquareShowAdapter(getContext(),listSquareShow);
        LinearLayoutManager linearLayoutManager1 = new GridLayoutManager(getContext(),7);
        rvSquareShow.setLayoutManager(linearLayoutManager1);
        rvSquareShow.setAdapter(squareShowAdapter);

        // Click phan tu nao mat phan tu day va dien len tren

        squareShowAdapter.setOnClickCallBack(new OnClickCallBack() {
            @Override
            public void onClick(int position) {
                s2+=listSquareShow.get(position);
                listSquareHidden.set(j,listSquareShow.get(position)+"");
                rvSquareHidden.setAdapter(squareHiddenAdapter);
                listSquareShow.remove(position);
                rvSquareShow.setAdapter(squareShowAdapter);
                j++;
                if(j==s.length()) {
                        onActionCallBack.onCallBack(KEY_HIGHSCORE,null);
                }
            }
        });


    }

    private void initViews() {
        imvBack = view.findViewById(R.id.imv_back);
        imvPicture = view.findViewById(R.id.imv_picture);
        rvSquareHidden = view.findViewById(R.id.rv_square_hidden);
        rvSquareShow = view.findViewById(R.id.rv_square_show);
        tvPoint = view.findViewById(R.id.tv_point);
    }

}
