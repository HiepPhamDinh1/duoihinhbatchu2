package com.example.duoihinhbatchu.view.frag;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.duoihinhbatchu.R;

public class M004HighScoreFragment extends Fragment {
    private TextView tvTrueFalse , tvRuby;
    private ImageView imvContinue;
    private View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.frag_m004_highscore,container,false);
        initViews();
        initEvents();
        return view;
    }

    private void initEvents() {
        if(M003MainFragment.s.equals(M003MainFragment.s2)){
            tvTrueFalse.setText("Quá tuyệt vời");
            tvRuby.setText("Bạn được tặng: +4 Ruby");
            M003MainFragment.point += 4;
        }else{
            tvTrueFalse.setText("Quá thất vọng");
            tvRuby.setText("Bạn bị trừ: -4 Ruby");
            M003MainFragment.point -= 4;
        }
        imvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                M003MainFragment.k++;
                getFragmentManager().popBackStack();
            }
        });
    }

    private void initViews() {
        tvTrueFalse = view.findViewById(R.id.tv_true_false);
        tvRuby = view.findViewById(R.id.tv_ruby);
        imvContinue = view.findViewById(R.id.imv_continue);
    }
}
