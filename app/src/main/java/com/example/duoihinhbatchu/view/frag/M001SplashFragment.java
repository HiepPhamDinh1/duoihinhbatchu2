package com.example.duoihinhbatchu.view.frag;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;

import com.example.duoihinhbatchu.OnActionCallBack;
import com.example.duoihinhbatchu.R;

public class M001SplashFragment extends Fragment {
    private static final long TIME_DELAY = 3000;
    public static final String KEY_MENUFRAGMENT = "KEY_MENUFRAGMENT";
    private View view;
    private OnActionCallBack onActionCallBack;

    public void setOnActionCallBack(OnActionCallBack onActionCallBack) {
        this.onActionCallBack = onActionCallBack;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_m001_splash,container,false);
        initViews();
        return view ;
    }

    private void initViews() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoMenuFragment();
            }
        },TIME_DELAY);
    }

    private void gotoMenuFragment() {
        onActionCallBack.onCallBack(KEY_MENUFRAGMENT,null);
    }
}
