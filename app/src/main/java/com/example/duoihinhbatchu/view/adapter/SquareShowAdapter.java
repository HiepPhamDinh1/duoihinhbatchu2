package com.example.duoihinhbatchu.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.duoihinhbatchu.OnClickCallBack;
import com.example.duoihinhbatchu.R;

import java.util.ArrayList;
import java.util.List;

public class SquareShowAdapter extends RecyclerView.Adapter<SquareShowAdapter.ViewHolder> {
    private Context context;
    private List<String> list = new ArrayList<>();
    private OnClickCallBack onClickCallBack;
    private int k =0;

    public void setOnClickCallBack(OnClickCallBack onClickCallBack) {
        this.onClickCallBack = onClickCallBack;
    }

    public SquareShowAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_square_show,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvSquareShow.setText(list.get(position));
        holder.llSquareShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCallBack.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSquareShow;
        private LinearLayout llSquareShow;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSquareShow = itemView.findViewById(R.id.tv_square_show);
            llSquareShow = itemView.findViewById(R.id.ll_square_show);
        }
    }
}
