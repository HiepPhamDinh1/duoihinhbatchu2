package com.example.duoihinhbatchu.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.duoihinhbatchu.R;

import java.util.ArrayList;
import java.util.List;

public class SquareHiddenAdapter extends RecyclerView.Adapter<SquareHiddenAdapter.ViewHolder> {
    private Context context;
    private List<String> list = new ArrayList<>();

    public SquareHiddenAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_square_hidden,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.tvSquareHidden.setText(list.get(position)+"");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSquareHidden;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSquareHidden = itemView.findViewById(R.id.tv_square_hidden);
        }
    }
}
