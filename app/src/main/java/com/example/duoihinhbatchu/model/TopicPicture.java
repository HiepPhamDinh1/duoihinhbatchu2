package com.example.duoihinhbatchu.model;

public class TopicPicture {
    private String mPicture;

    public TopicPicture(String mPicture) {
        this.mPicture = mPicture;
    }

    public String getmPicture() {
        return mPicture;
    }

    public void setmPicture(String mPicture) {
        this.mPicture = mPicture;
    }
}
