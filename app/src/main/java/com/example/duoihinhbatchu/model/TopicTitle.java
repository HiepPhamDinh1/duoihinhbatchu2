package com.example.duoihinhbatchu.model;

public class TopicTitle {
    private String mTitle;

    public TopicTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}
